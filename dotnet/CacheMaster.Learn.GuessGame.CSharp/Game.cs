using static System.Math;


namespace CacheMaster.Learn.GuessGame.CSharp
{
    public sealed class Game
    {
        private readonly int _answer;
        private readonly int _attempts;


        public Game(int lo, int hi, int level)
        {
            var intervalLength = hi - lo + 1;
            var _attempts = Log(intervalLength, 2);
            //var answer = 
        }


        public int Attempts {get;}


        public GameResult? Play(IUserInteractor interactor, IBinarySearchDetector binarySearchDetector)
        {
            var attemptsLeft = _attempts;
            var usedBinarySearch = true;

            while ((attemptsLeft--) > 0)
            {
                var turn = _attempts - attemptsLeft;
                var guess = interactor.GetGuess(turn);
                var ord = guess.CompareTo(_answer);
                

                interactor.PrintTurnResult(turn, ord);

                if (ord == 0)
                    return new GameResult();
            }

            return new GameResult();
        }
    }
}