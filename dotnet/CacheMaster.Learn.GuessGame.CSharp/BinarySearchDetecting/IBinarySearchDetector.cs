namespace CacheMaster.Learn.GuessGame.CSharp
{
    public interface IBinarySearchDetector
    {
        bool CheckTurn(int guess, int lo, int hi);
    }
}