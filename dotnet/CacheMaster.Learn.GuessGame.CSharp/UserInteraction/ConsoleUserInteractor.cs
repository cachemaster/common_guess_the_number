using static System.Console;
using static System.Int32;
using static System.Math;


namespace CacheMaster.Learn.GuessGame.CSharp
{
    public class ConsoleUserInteractor : IUserInteractor
    {
        const string TooBigMessage = "";
        const string TooSmallMessage = "";
        const string EqualsMessage = "";


        public int GetGuess(int turn)
        {
            var result = 0;

            do
            {
                WriteLine($"[{turn}] Input your guess: ");
            }
            while (!TryParse(ReadLine(), out result));

            return result;
        }

        public void PrintTurnResult(int turn, int ord)
        {
            var message = ord < 0
                ? TooSmallMessage
                : ord > 0
                    ? TooBigMessage
                    : EqualsMessage;
            
            WriteLine(message);
        }

        public bool ConfirmOptions(int lo, int hi, int attempts) => true;
    }
}