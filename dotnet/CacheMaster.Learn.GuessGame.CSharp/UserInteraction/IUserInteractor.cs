namespace CacheMaster.Learn.GuessGame.CSharp
{
    public interface IUserInteractor
    {
        int GetGuess(int turn);

        void PrintTurnResult(int turn, int ord);

        bool ConfirmOptions(int lo, int hi, int attempts);
    }
}